/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/01 14:42:52 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/01 18:34:30 by rblondia         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h>

void	ft_putchar(char c)
{
	write (1, &c, 1);
}

void	display(int i)
{
	int	first;
	int	second;

	first = i / 10;
	second = i % 10;
	ft_putchar(first + '0');
	ft_putchar(second + '0');
}

void	ft_print_comb2(void)
{
	int	f;
	int	s;

	f = 0;
	while (f < 99)
	{
		s = f + 1;
		while (s <= 99)
		{
			display(f);
			ft_putchar(' ');
			display(s);
			if (f < 98 || s < 99)
			{
				ft_putchar(',');
				ft_putchar(' ');
			}
			s++;
		}
		f++;
	}
}
