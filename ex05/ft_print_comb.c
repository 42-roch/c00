/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/01 14:42:52 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/02 14:20:39 by rblondia         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h>

void	ft_putchar(char c)
{
	write (1, &c, 1);
}

void	ft_print(int i, int j, int k)
{
	ft_putchar(i);
	ft_putchar(j);
	ft_putchar(k);
}

void	ft_print_comb(void)
{
	int	i;
	int	j;
	int	k;

	i = '0';
	while (i <= ('6' + 1))
	{
		j = i + 1;
		while (j <= ('7' + 1))
		{
			k = j + 1;
			while (k <= ('8' + 1))
			{
				ft_print(i, j, k);
				if (i != ('6' + 1) || j != ('7' + 1) || k != ('8' + 1))
				{
					ft_putchar(',');
					ft_putchar(' ');
				}
				k += 1;
			}
			j += 1;
		}
		i += 1;
	}
}
